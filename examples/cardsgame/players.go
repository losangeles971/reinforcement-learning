package cardsgame

import "fmt"

type Player interface {
	Name() string
	IsHuman() bool
	SetHand(h []Card)
	GetCard(i int) Card
	DoYouHave(c Card) bool
	PopCard(c Card)
	Play(table Table) Card
	Size() int
	AddCardToHand(c Card) error
	TellYouHandResult(table Table, result bool)
	TellYouMatchResult(result bool)
}

type AbstractPlayer struct {
	hand  []Card
}

func (p *AbstractPlayer) SetHand(h []Card) {
	p.hand = h
}

func (p *AbstractPlayer) GetCard(i int) Card {
	return p.hand[i]
}

func (p *AbstractPlayer) DoYouHave(c Card) bool {
	for i := range p.hand {
		if p.hand[i].isEqualTo(c) {
			return true
		}
	}
	return false
}

func (p *AbstractPlayer) PopCard(c Card) {
	h := []Card{}
	for i := range p.hand {
		if !p.hand[i].isEqualTo(c) {
			h = append(h, p.hand[i])
		}
	}
	p.SetHand(h)
}

func (p *AbstractPlayer) Size() int {
	return len(p.hand)
}

func (p *AbstractPlayer) AddCardToHand(c Card) error {
	if p.DoYouHave(c) {
		return fmt.Errorf("player alredy has card %s", c.Label)
	}
	p.hand = append(p.hand, c)
	return nil
}

type TestPlayer struct {
	AbstractPlayer
	human bool
}

func NewTestPlayer(human bool) *TestPlayer {
	return &TestPlayer{
		human: human,
	}
}

func (p *TestPlayer) Name() string {
	return "Test Player"
}

func (p *TestPlayer) IsHuman() bool {
	return p.human
}

func (p *TestPlayer) Play(table Table) Card {
	c := p.GetCard(0)
	p.PopCard(c)
	return c
}

func (p *TestPlayer) TellYouHandResult(table Table, result bool) {
}

func (p *TestPlayer) TellYouMatchResult(result bool) {
}
