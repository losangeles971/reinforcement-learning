package cardsgame

import (
	_ "embed"
)

type Card struct {
	Seed  int `json:"seed"`
	Value int `json:"value"`
	Label string `json:"label"`
}

func (c Card) clone() Card {
	return Card{
		Seed: c.Seed,
		Value: c.Value,
		Label: c.Label,
	}
}

func (c Card) isEqualTo(b Card) bool {
	if c.Seed == b.Seed && c.Value == b.Value {
		return true
	}
	return false
}