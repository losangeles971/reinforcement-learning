package cardsgame

import (
	"testing"
)

func TestDeck(t * testing.T) {
	d, err := LoadDeck()
	if err != nil {
		t.Fatal(err)
	}
	size := 40
	if d.Size() != size {
		t.Fatalf("wrong size of new deck %v expected %v", d.Size(), size)
	}
	for i := 0; i < size; i++ {
		c, err := d.Random()
		if err != nil {
			t.Fatal(err)
		}
		if d.DoYouHave(c) {
			t.Fatal("deck cannot have popped card")
		}
	}
	if d.Size() != 0 {
		t.Fatalf("wrong size of new deck %v at the end of popping", d.Size())
	}
}