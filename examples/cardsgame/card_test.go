package cardsgame

import (
	"testing"
)

var hand = []Card{
	{
		Seed: 0,
		Value: 0,
	},
	{
		Seed: 1,
		Value: 0,
	},
	{
		Seed: 0,
		Value: 1,
	},
}

func TestCard(t * testing.T) {
	if hand[0].isEqualTo(hand[0].clone()) != true {
		t.Fatal("a card must be equal to its clone")
	}
	if hand[0].isEqualTo(hand[1]) == true {
		t.Fatal("card a cannot be equal to b")
	}
	if hand[0].isEqualTo(hand[2]) == true {
		t.Fatal("card a cannot be equal to c")
	}
}