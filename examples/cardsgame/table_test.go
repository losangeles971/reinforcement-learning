package cardsgame

import (
	"testing"
)

func TestTable(t * testing.T) {
	table := NewTable()
	for i := range locations {
		l, err := table.GetLocation(locations[i])
		if err != nil {
			t.Fatal(err)
		}
		if l.occupied {
			t.Errorf("location %s cannot be occupied", l.name)
		}
	}
	for i := 0; i < 3; i++ {
		table.SetCard(locations[i], hand[i])
	}
	for i := 0; i < 3; i++ {
		l, err := table.GetLocation(locations[i])
		if err != nil {
			t.Fatal(err)
		}
		if !l.occupied {
			t.Errorf("location %s must be occupied", l.name)
		}
	}
	for i := 0; i < 3; i++ {
		err := table.RemoveCard(locations[i])
		if err != nil {
			t.Fatal(err)
		}
	}
	for i := range locations {
		l, err := table.GetLocation(locations[i])
		if err != nil {
			t.Fatal(err)
		}
		if l.occupied {
			t.Errorf("location %s cannot be occupied", l.name)
		}
	}
}