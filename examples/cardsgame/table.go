package cardsgame

import (
	"fmt"

	"github.com/sirupsen/logrus"
)

const (
	NORD   = "Nord"
	SUD    = "Sud"
	EST    = "Est"
	OVEST  = "Ovest"
	CENTER = "Center"
)

var locations = []string{NORD, SUD, EST, OVEST, CENTER, }

type Location struct {
	name     string
	occupied bool
	c        Card
}

func (l Location) IsOccupied() bool {
	return l.occupied
}

func (l Location) GetCard() (Card, error) {
	if !l.occupied {
		return Card{}, fmt.Errorf("location %s is not occupied", l.name)
	}
	return l.c, nil
}

type Table struct {
	state     []*Location
}

func NewTable() *Table {
	s := []*Location{}
	for i := range locations {
		s = append(s, &Location{
			name:     locations[i],
			occupied: false,
			c:        Card{},
		})
	}
	return &Table{
		state:     s,
	}
}

func (t *Table) IsValidLocation(location string) bool {
	for i := range t.state {
		if t.state[i].name == location {
			return true
		}
	}
	return false
}

func (t *Table) GetLocation(location string) (*Location, error) {
	if !t.IsValidLocation(location) {
		return nil, fmt.Errorf("location %s is not valid", location)
	}
	for i := range t.state {
		if t.state[i].name == location {
			return t.state[i], nil
		}
	}
	return nil, fmt.Errorf("location %s not found", location)
}

func (t *Table) SetCard(location string, c Card) error {
	l, err := t.GetLocation(location)
	if err != nil {
		return err
	}
	if l.occupied {
		return fmt.Errorf("location %s occupied by card %s", location, l.c.Label)
	}
	l.occupied = true
	l.c = c
	return nil
}

func (t *Table) GetCard(location string) (Card, error) {
	l, err := t.GetLocation(location)
	if err != nil {
		return Card{}, err
	}
	return l.GetCard()
}

func (t *Table) RemoveCard(location string) error {
	l, err := t.GetLocation(location)
	if err != nil {
		return err
	}
	if !l.occupied {
		return fmt.Errorf("location %s is not occupied", location)
	}
	l.occupied = false
	return nil
}

func (t *Table) getLabel(location string) string {
	l, err := t.GetLocation(location)
	if err != nil {
		logrus.Fatalf("localtion %s not existent -> %v", location, err)
	}
	if l.IsOccupied() {
		c, err := l.GetCard()
		if err != nil {
			logrus.Fatalf("localtion %s is occupied by no card -> %v", location, err)
		}
		return c.Label
	}
	return ""
}

func (t *Table) Print() {
	ClearScreen()
	fmt.Print("+------------------------------------------------+ \n")
	fmt.Printf("                     [ %s ] \n", t.getLabel(NORD))
	fmt.Print("\n\n\n\n")
	fmt.Printf("                 { %s } \n", t.getLabel(CENTER))
	fmt.Print("\n\n\n\n")
	fmt.Printf("                     [ %s ] \n", t.getLabel(SUD))
	fmt.Print("+------------------------------------------------+ \n")
}
