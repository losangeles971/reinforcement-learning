package cardsgame

import (
	"github.com/looplab/fsm"
)

const (
	common_state_init = "init"
	common_state_end = "end"
)

type Match struct {
	machine *fsm.FSM
}

func NewBriscolaFSM() *Match {
	m := &Match{}
	machine := fsm.NewFSM(
        "init",
        fsm.Events{
            {Name: "prepare", Src: []string{common_state_init}, Dst: "empty"},
            {Name: "attack", Src: []string{"empty"}, Dst: "half"},
			{Name: "respond", Src: []string{"half"}, Dst: "challenge"},
			{Name: "onecard", Src: []string{"challenge"}, Dst: "empty"},
			{Name: "close", Src: []string{"challenge"}, Dst: common_state_end},
        },
        fsm.Callbacks{
			"exec_state": func(e *fsm.Event) { m.execState(e) },
        },
    )
	m.machine = machine
	return m
}

func (m *Match) execState(e *fsm.Event) {

}