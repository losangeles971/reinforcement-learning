package cardsgame

import (
	"testing"
)

func TestPlayers(t * testing.T) {
	p := NewTestPlayer(false)
	p.SetHand(hand)
	if p.Size() != len(hand) {
		t.Errorf("player size must be %v not %v", len(hand), p.Size())
	}
	if !p.DoYouHave(hand[0]) {
		t.Fatalf("player must have card %s", hand[0].Label)
	}
	c := p.Play(Table{})
	if p.DoYouHave(c) {
		t.Errorf("player must NOT have card [%s]", c.Label)
	}
	if p.Size() != len(hand) - 1 {
		t.Errorf("player size must be %v not %v", len(hand) - 1, p.Size())
	}
}

func TestAddCardToPlayer(t * testing.T) {
	p := NewTestPlayer(false)
	err := p.AddCardToHand(hand[0])
	if err != nil {
		t.Fatal(err)
	}
	if !p.DoYouHave(hand[0]) {
		t.Fatalf("player must have card %s", hand[0].Label)
	}
}