package cardsgame

import (
	"crypto/rand"
	_ "embed"
	"encoding/json"
	"errors"
	"math/big"
)

//go:embed italiane.json
var italiane string

type Deck struct {
	Cards []Card `json:"cards"`
}

func LoadDeck() (*Deck, error) {
	d := &Deck{}
	err := json.Unmarshal([]byte(italiane), d)
	return d, err
}

func (d *Deck) Size() int {
	return len(d.Cards)
}

func (d *Deck) remove(i int) {
    d.Cards[i] = d.Cards[d.Size() - 1].clone()
	d.Cards = d.Cards[:d.Size() - 1]
}

func (d *Deck) Random() (Card, error) {
	if d.Size() < 1 {
		return Card{}, errors.New("already popped all cards from the deck")
	}
	i, err := rand.Int(rand.Reader, big.NewInt(int64(d.Size())))
	if err != nil {
		return Card{}, err
	}
	c := d.Cards[i.Int64()]
	d.remove(int(i.Int64()))
	return c, nil
}

func (d *Deck) DoYouHave(c Card) bool {
	for i := range d.Cards {
		if d.Cards[i].isEqualTo(c) {
			return true
		}
	}
	return false
}