package briscola

import (
	"bufio"
	"crypto/rand"
	"fmt"
	"losangeles971/rl/examples/cardsgame"
	"math/big"
	"os"
	"strconv"
	"strings"
)

var cardPower = map[int]int{
	1:  10,
	3:  9,
	10: 8,
	9:  7,
	8:  6,
	7:  5,
	6:  4,
	5:  3,
	4:  2,
	2:  1,
}
var CardPoints = map[int]int{
	1:  11,
	3:  10,
	10: 4,
	9:  3,
	8:  2,
	7:  0,
	6:  0,
	5:  0,
	4:  0,
	2:  0,
}

type Briscola struct {
	deck       *cardsgame.Deck
	table      *cardsgame.Table
	nord       cardsgame.Player
	sud        cardsgame.Player
	briscola   cardsgame.Card
	next       string
	nordPoints int
	sudPoints  int
	reader     *bufio.Reader
	stepper    bool
}

func New(stepper bool) (*Briscola, error) {
	d, err := cardsgame.LoadDeck()
	if err != nil {
		return nil, err
	}
	b := &Briscola{
		deck:       d,
		table:      cardsgame.NewTable(),
		nord:       nil,
		sud:        nil,
		next:       "",
		nordPoints: 0,
		sudPoints:  0,
		reader:     bufio.NewReader(os.Stdin),
		stepper:    stepper,
	}
	return b, nil
}

func (b *Briscola) AddPlayerToLocation(p cardsgame.Player, location string) error {
	if !b.table.IsValidLocation(location) && location != cardsgame.CENTER {
		return fmt.Errorf("location %s is not valid", location)
	}
	switch location {
	case cardsgame.NORD:
		b.nord = p
	default:
		b.sud = p
	}
	return nil
}

func (b *Briscola) getHand() ([]cardsgame.Card, error) {
	if b.deck.Size() < 3 {
		return nil, fmt.Errorf("cannot give you a new hand, deck got only %v cards", b.deck.Size())
	}
	h := []cardsgame.Card{}
	for i := 0; i < 3; i++ {
		c, err := b.deck.Random()
		if err != nil {
			return nil, err
		}
		h = append(h, c)
	}
	return h, nil
}

func (b *Briscola) getCard() (cardsgame.Card, error) {
	if b.deck.Size() < 1 {
		return b.briscola, nil
	}
	return b.deck.Random()
}

func (b *Briscola) Prepare() error {
	var err error
	b.briscola, err = b.deck.Random()
	if err != nil {
		return err
	}
	b.table.SetCard(cardsgame.CENTER, b.briscola)
	h, err := b.getHand()
	if err != nil {
		return err
	}
	b.nord.SetHand(h)
	h, err = b.getHand()
	if err != nil {
		return err
	}
	b.sud.SetHand(h)
	i, err := rand.Int(rand.Reader, big.NewInt(2))
	if err != nil {
		return err
	}
	if i.Int64() == 0 {
		b.next = cardsgame.NORD
	} else {
		b.next = cardsgame.SUD
	}
	return nil
}

func (b *Briscola) firstWon(first cardsgame.Card, second cardsgame.Card) bool {
	if first.Seed == second.Seed {
		if cardPower[first.Value] > cardPower[second.Value] {
			return true
		} else {
			return false
		}
	}
	if first.Seed == b.briscola.Seed {
		return true
	}
	if second.Seed == b.briscola.Seed {
		return false
	}
	return true
}

func (b *Briscola) play(p cardsgame.Player, location string) (cardsgame.Card, error) {
	if p.IsHuman() {
		fmt.Printf("Human player %s choose: \n", p.Name())
		for i := 0; i < p.Size(); i++ {
			fmt.Printf("%v - %s\n", i, p.GetCard(i).Label)
		}
		data, _ := b.reader.ReadString('\n')
		choice, err := strconv.Atoi(strings.TrimSpace(data))
		if err != nil {
			return cardsgame.Card{}, err
		}
		c := p.GetCard(choice)
		p.PopCard(c)
		fmt.Printf("-> %s\n", c.Label)
		return c, nil
	} else {
		return p.Play(*b.table), nil
	}
}

func (b *Briscola) PrintWinner() error {
	fmt.Print("**************************************************\n")
	fmt.Printf("nord points %v and sud points %v\n", b.nordPoints, b.sudPoints)
	if b.nordPoints > b.sudPoints {
		fmt.Printf("The winner is %s\n", b.nord.Name())
	}
	if b.nordPoints < b.sudPoints {
		fmt.Printf("The winner is %s\n", b.sud.Name())
	}
	fmt.Print("**************************************************\n")
	return nil
}

func (b *Briscola) Round() error {
	if b.nord.Size() != b.sud.Size() {
		return fmt.Errorf("players have different number of cards, nord %v sud %v", b.nord.Size(), b.sud.Size())
	}
	b.table.Print()
	if b.nord.Size() < 3 && b.deck.Size() >= 1 {
		c1, err := b.getCard()
		if err != nil {
			return err
		}
		var c2 cardsgame.Card
		if b.deck.Size() == 0 {
			c2 = b.briscola
		} else {
			c2, err = b.getCard()
			if err != nil {
				return err
			}
		}
		if b.next == cardsgame.NORD {
			b.nord.AddCardToHand(c1)
			b.sud.AddCardToHand(c2)
		} else {
			b.nord.AddCardToHand(c2)
			b.sud.AddCardToHand(c1)
		}
	}
	if b.next == cardsgame.NORD {
		first, err := b.play(b.nord, cardsgame.NORD)
		if err != nil {
			return err
		}
		err = b.table.SetCard(cardsgame.NORD, first)
		if err != nil {
			return err
		}
		b.table.Print()
		second, err := b.play(b.sud, cardsgame.SUD)
		if err != nil {
			return err
		}
		err = b.table.SetCard(cardsgame.SUD, second)
		if err != nil {
			return err
		}
		b.table.Print()
		if b.firstWon(first, second) {
			b.next = cardsgame.NORD
			b.nordPoints += CardPoints[first.Value]
			b.nordPoints += CardPoints[second.Value]
			b.nord.TellYouHandResult(*b.table, true)
			b.sud.TellYouHandResult(*b.table, false)
		} else {
			b.next = cardsgame.SUD
			b.sudPoints += CardPoints[first.Value]
			b.sudPoints += CardPoints[second.Value]
			b.nord.TellYouHandResult(*b.table, false)
			b.sud.TellYouHandResult(*b.table, true)
		}
		err = b.table.RemoveCard(cardsgame.NORD)
		if err != nil {
			return err
		}
		err = b.table.RemoveCard(cardsgame.SUD)
		if err != nil {
			return err
		}
	} else {
		first, err := b.play(b.sud, cardsgame.SUD)
		if err != nil {
			return err
		}
		err = b.table.SetCard(cardsgame.SUD, first)
		if err != nil {
			return err
		}
		b.table.Print()
		second, err := b.play(b.nord, cardsgame.NORD)
		if err != nil {
			return err
		}
		err = b.table.SetCard(cardsgame.NORD, second)
		if err != nil {
			return err
		}
		b.table.Print()
		if b.firstWon(first, second) {
			b.next = cardsgame.SUD
			b.sudPoints += CardPoints[first.Value]
			b.sudPoints += CardPoints[second.Value]
			b.nord.TellYouHandResult(*b.table, false)
			b.sud.TellYouHandResult(*b.table, true)
		} else {
			b.next = cardsgame.NORD
			b.nordPoints += CardPoints[first.Value]
			b.nordPoints += CardPoints[second.Value]
			b.nord.TellYouHandResult(*b.table, true)
			b.sud.TellYouHandResult(*b.table, false)
		}
		err = b.table.RemoveCard(cardsgame.NORD)
		if err != nil {
			return err
		}
		err = b.table.RemoveCard(cardsgame.SUD)
		if err != nil {
			return err
		}
	}
	b.table.Print()
	if b.stepper {
		fmt.Println("press enter to continue")
		b.reader.ReadString('\n')
	}
	return nil
}

func (b *Briscola) Winners() []cardsgame.Player {
	if b.nordPoints == b.sudPoints {
		return []cardsgame.Player{b.nord, b.sud}
	}
	if b.nordPoints < b.sudPoints {
		return []cardsgame.Player{b.sud}
	}
	return []cardsgame.Player{b.nord}
}

func (b *Briscola) Run() error {
	for i := 0; i < 20; i++ {
		err := b.Round()
		if err != nil {
			return err
		}
	}
	return nil
}