package briscola

import (
	"losangeles971/rl/examples/cardsgame"
	"testing"
)

func TestPrepare(t * testing.T) {
	nordPlayer := cardsgame.NewTestPlayer(false)
    sudPlayer := cardsgame.NewTestPlayer(false)
	b, err := New(false)
	if err != nil {
		t.Fatal(err)
	}
	err = b.AddPlayerToLocation(nordPlayer, cardsgame.NORD)
	if err != nil {
		t.Fatal(err)
	}
	err = b.AddPlayerToLocation(sudPlayer, cardsgame.SUD)
	if err != nil {
		t.Fatal(err)
	}
	err = b.Prepare()
	if err != nil {
		t.Fatal(err)
	}
	if nordPlayer.Size() != 3 {
		t.Errorf("player %s should have 3 cards not %v", nordPlayer.Name(), nordPlayer.Size())
	}
	if sudPlayer.Size() != 3 {
		t.Errorf("player %s should have 3 cards not %v", sudPlayer.Name(), sudPlayer.Size())
	}
	if b.next == "" {
		t.Fatal("missing next player")
	}
}

func TestMatch(t * testing.T) {
	nordPlayer := cardsgame.NewTestPlayer(false)
    sudPlayer := cardsgame.NewTestPlayer(false)
	b, err := New(false)
	if err != nil {
		t.Fatal(err)
	}
	err = b.AddPlayerToLocation(nordPlayer, cardsgame.NORD)
	if err != nil {
		t.Fatal(err)
	}
	err = b.AddPlayerToLocation(sudPlayer, cardsgame.SUD)
	if err != nil {
		t.Fatal(err)
	}
	err = b.Prepare()
	if err != nil {
		t.Fatal(err)
	}
	for i := 0; i < 20; i++ {
		err := b.Round()
		if err != nil {
			t.Fatalf("error at round %v -> %v", i, err)
		}
	}
	if b.nordPoints + b.sudPoints != 120 {
		t.Fatalf("wrong points, nord %v sud %b", b.nordPoints, b.sudPoints)
	}
}