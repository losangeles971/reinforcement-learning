package pdor

import (
	"fmt"
	"losangeles971/rl/business/qlearner"
	"losangeles971/rl/examples/briscola"
	"losangeles971/rl/examples/cardsgame"
	"os"

	"github.com/sirupsen/logrus"
)

var actions = []string{
	"briscola",
	"scarto",
	"carico",
}

const (
	pdor_filepath = "pdor.json"
)

type state struct {
	name      string
	attacking bool
	briscola  int
	action    string
	mycard    cardsgame.Card
	won       bool
}

type Pdor struct {
	cardsgame.AbstractPlayer
	q       *qlearner.QLearner
	points  int
	history []*state
}

func getState(b int, p int, s int, c int) string {
	return fmt.Sprintf("%v,%v,%v,%v", b, p, s, c)
}

func New() *Pdor {
	states := []string{}
	for b := 0; b < 4; b++ {
		for p := 0; p < 121; p++ {
			for s := 0; s < 4; s++ {
				for c := 0; c < 4; c++ {
					states = append(states, getState(b, p, s, c))
				}
			}
		}
	}
	p := Pdor{
		q:       qlearner.NewQLearner(
			states, 
			actions, 
			qlearner.WithAlfa(0.5),
			qlearner.WithEpsilon(0.4),
			qlearner.WithGamma(0.8),
			qlearner.WithFile(pdor_filepath),
		),
		history: []*state{},
	}
	if _, err := os.Stat(pdor_filepath); err == nil {
		err := p.q.Load()
		if err != nil {
			fmt.Printf("experience file of Pdor %s does not exist [%v]\n", pdor_filepath, err)
		}
	} else {
		fmt.Printf("missing experience file of Pdor %s \n", pdor_filepath)
	}
	return &p
}

func (p *Pdor) Name() string {
	return "Pdor"
}

func (p *Pdor) IsHuman() bool {
	return false
}

func (p *Pdor) briscola(briscola int) (cardsgame.Card, bool) {
	for i := 0; i < p.Size(); i++ {
		cd := p.GetCard(i)
		if cd.Seed == briscola {
			p.PopCard(cd)
			return cd, true
		}
	}
	return cardsgame.Card{}, false
}

func (p *Pdor) carico(briscola int) (cardsgame.Card, bool) {
	for i := 0; i < p.Size(); i++ {
		cd := p.GetCard(i)
		if cd.Seed != briscola {
			if cd.Value == 1 || cd.Value == 3 {
				p.PopCard(cd)
				return cd, true
			}
		}
	}
	return cardsgame.Card{}, false
}

func (p *Pdor) scarto(briscola int) (cardsgame.Card, bool) {
	for i := 0; i < p.Size(); i++ {
		cd := p.GetCard(i)
		if cd.Seed != briscola && cd.Value != 1 && cd.Value != 3 && cd.Value < 8 {
			p.PopCard(cd)
			return cd, true
		}
	}
	return cardsgame.Card{}, false
}

func (p *Pdor) Play(table cardsgame.Table) cardsgame.Card {
	l, _ := table.GetLocation(cardsgame.CENTER)
	briscola, _ := l.GetCard()
	s := 0
	c := 0
	for i := 0; i < p.Size(); i++ {
		cd := p.GetCard(i)
		if cd.Value == 1 || cd.Value == 3 {
			c++
		} else if cd.Value < 8 {
			s++
		}
	}
	last := &state{
		name:      getState(briscola.Seed, p.points, s, c),
		attacking: true,
		briscola:  briscola.Seed,
	}
	var err error
	last.action, err = p.q.Policy(last.name)
	if err != nil {
		logrus.Errorf("policy failed -> %v", err)
	}
	logrus.Debugf("Pdor chose [ %v ]", last.action)
	var played_card *cardsgame.Card
	switch last.action {
	case "briscola":
		cd, ok := p.briscola(briscola.Seed)
		if ok {
			played_card = &cd
		}
	case "scarto":
		cd, ok := p.scarto(briscola.Seed)
		if ok {
			played_card = &cd
		}
	case "carico":
		cd, ok := p.carico(briscola.Seed)
		if ok {
			played_card = &cd
		}
	}
	if played_card == nil {
		cd := p.GetCard(0)
		p.PopCard(cd)
		played_card = &cd
	}
	last.mycard = *played_card
	p.history = append(p.history, last)
	return last.mycard
}

func (p *Pdor) TellYouHandResult(table cardsgame.Table, result bool) {
	last := p.history[len(p.history)-1]
	last.won = result
	if len(p.history) > 1 {
		previous := p.history[len(p.history)-2]
		nord, _ := table.GetLocation(cardsgame.NORD)
		sud, _ := table.GetLocation(cardsgame.SUD)
		c1, _ := nord.GetCard()
		c2, _ := sud.GetCard()
		point := briscola.CardPoints[c1.Value] + briscola.CardPoints[c2.Value]
		var reward = 0.0
		if result {
			if point == 0 {
				reward = 0.4
			} else {
				reward = 0.5
			}
		} else {
			if point == 0 {
				reward = 0.3
			} else {
				reward = 0.1
			}
		}
		logrus.Debugf("Pdor is learning from [ %s ][ %s ]-[ %s ]>[ %v ]", previous.name, last.action, last.name, reward)
		err := p.q.Learn(previous.name, last.action, last.name, reward)
		if err != nil {
			logrus.Errorf("Pdor is NOT learning -> [ %v ]", err)
		}
	}
}

func (p *Pdor) TellYouMatchResult(result bool) {
	if len(p.history) > 1 {
		last := p.history[len(p.history)-1]
		previous := p.history[len(p.history)-2]
		var reward = 0.0
		if result {
			reward = 1.0
		} else {
			reward = 0.0
		}
		err := p.q.Learn(previous.name, last.action, last.name, reward)
		if err != nil {
			logrus.Errorf("Pdor is NOT learning -> [ %v ]", err)
		}
	}
}

func (p *Pdor) Save() error {
	return p.q.Save()
}