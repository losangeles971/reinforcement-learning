package sudoku

import (
	"log"
	"math/rand"
	"testing"
)

var valid_solution = Sudoku{
	matrix: [ROWS][COLUMNS]int{
		{4, 3, 5, 2, 6, 9, 7, 8, 1},
		{6, 8, 2, 5, 7, 1, 4, 9, 3},
		{1, 9, 7, 8, 3, 4, 5, 6, 2},
		{8, 2, 6, 1, 9, 5, 3, 4, 7},
		{3, 7, 4, 6, 8, 2, 9, 1, 5},
		{9, 5, 1, 7, 4, 3, 6, 2, 8},
		{5, 1, 9, 3, 2, 6, 8, 7, 4},
		{2, 4, 8, 9, 5, 7, 1, 3, 6},
		{7, 6, 3, 4, 1, 8, 2, 5, 9},
	},
}

var empty_sudoku = "000000000000000000000000000000000000000000000000000000000000000000000000000000000"

func TestGet(t *testing.T) {
	v := valid_solution.Get(3, 2)
	if v != 8 {
		log.Panic("get is not working")
		t.FailNow()
	}
}

func TestLoad(t *testing.T) {
	s, _ := LoadByLine(easy_sudoku)
	if len(s.history) != 0 {
		log.Fatalf("expected initial len of history 0 instead of %v", len(s.history))
		t.FailNow()
	}
}

func TestHistory(t *testing.T) {
	s, _ := LoadByLine(easy_sudoku)
	if len(s.history) != 0 {
		log.Fatalf("expected initial len of history 0 instead of %v", len(s.history))
		t.FailNow()
	}
	moved := s.set(Move{X: 0, Y: 0, V: 8}, false)
	if !moved {
		log.Fatal("expected a move")
		t.FailNow()
	}
	if len(s.history) != 1 {
		log.Fatalf("expected len of history 1 instead of %v", len(s.history))
		t.FailNow()
	}
	if s.moves != 1 {
		log.Fatalf("expected 1 total moves instead of %v", s.moves)
		t.FailNow()
	}
	moved = s.set(Move{X: 0, Y: 0, V: 0}, false) // undo
	if !moved {
		log.Fatal("expected an undo move")
		t.FailNow()
	}
	if s.Get(0, 0) != 0 {
		log.Fatalf("undo failed, expected 0 instead of %v", s.Get(0, 0))
		t.FailNow()
	}
	if s.moves != 2 {
		log.Fatalf("expected 2 total moves instead of %v", s.moves)
		t.FailNow()
	}
	moved = s.set(Move{X: 0, Y: 0, V: 12}, false) // wrong move
	if moved {
		log.Fatal("expected move due to invalid value for the cell")
		t.FailNow()
	}
}

func TestCheck(t *testing.T) {
	var s Sudoku
	for i := 0; i < 100; i++ {
		s, _ = LoadByLine(empty_sudoku)
		// wrong data on a column
		x := rand.Intn(9)
		s.set(Move{X: x, Y: rand.Intn(5), V: 3}, false)
		s.set(Move{X: x, Y: 5 + rand.Intn(4), V: 3}, false)
		vv := s.Check()
		if vv != INVALID_COLUMN {
			log.Panicf("check is not working on columns: %d", vv)
			t.FailNow()
		}
		s, _ = LoadByLine(empty_sudoku)
		// wrong data on a row
		y := rand.Intn(9)
		s.set(Move{X: rand.Intn(5), Y: y, V: 3}, false)
		s.set(Move{X: 5 + rand.Intn(4), Y: y, V: 3}, false)
		vv = s.Check()
		if vv != INVALID_ROW {
			log.Panicf("check is not working on rows: %d", vv)
			t.FailNow()
		}
	}
	s, _ = LoadByLine(empty_sudoku)
	s.set(Move{X: 0, Y: 0, V: 1}, false)
	s.set(Move{X: 2, Y: 2, V: 1}, false)
	vv := s.Check()
	if vv != INVALID_BLOCK {
		log.Panicf("check is not working on blocks: %d", vv)
		t.FailNow()
	}
}

func TestClone(t *testing.T) {
	s, _ := LoadByLine(empty_sudoku)
	m := Move{
		X: rand.Intn(9),
		Y: rand.Intn(9),
		V: rand.Intn(9) + 1,
	}
	s.set(m, false)
	log.Println("original")
	s.Print()
	cloned := s.Clone()
	log.Println("cloned")
	cloned.Print()
	if cloned.Get(m.X, m.Y) != m.V {
		log.Fatal("clone is not working")
		t.FailNow()
	}
}

func TestValidSolution(t *testing.T) {
	vv := valid_solution.Check()
	if vv != COMPLETED {
		log.Fatal("checking valid solution is not working")
		t.FailNow()
	}
}

func TestLoadByLine(t *testing.T) {
	s, err := LoadByLine("012345678987654321012345678987654321012345678987654321012345678987654321012345678")
	if err != nil {
		log.Fatal(err)
		t.FailNow()
	}
	if s.get0(2, 0) != 2 {
		log.Fatal("loading by line corrupts data")
		t.FailNow()
	}
	if s.Get(2, 0) != 2 {
		log.Fatal("loading by line corrupts data")
		t.FailNow()
	}
}

func TestGetMoves(t *testing.T) {
	//000260701
	//680070090
	//190004500
	//820100040
	//004602900
	//050003028
	//009300074
	//040050036
	//703018000
	grid, _ := LoadByLine(easy_sudoku)
	moves := grid.GetMoves(0, 0)
	// 3,4,5
	if len(moves) != 3 {
		log.Printf("get moves is not working expected 3 moves instead of %v", len(moves))
	}
	if moves[0] != 3 {
		log.Print("get moves is not working expected move: 3")
	}
	if moves[1] != 4 {
		log.Print("get moves is not working expected move: 4")
	}
	if moves[2] != 5 {
		log.Print("get moves is not working expected move: 5")
	}
}
