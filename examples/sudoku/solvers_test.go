package sudoku

import (
	"log"
	"testing"
)

const (
	BAD_MOVE   = 0
	GUESS_MOVE = 1
	NO_MOVE    = 2
)

var easy_sudoku = "000260701680070090190004500820100040004602900050003028009300074040050036703018000"

/*
var batch = []string{
	"001700509573024106800501002700295018009400305652800007465080071000159004908007053",
	"029650178705180600100900500257800309600219005900007046573400021800020453010395000",
}
*/

type TestSolver struct {
	type_of_move int
}

func (solver TestSolver) Save(filepath string) error {
	return nil
}

func (solver TestSolver) Load(filepath string) error {
	return nil
}

func (solver TestSolver) GetMove(grid Sudoku) (Move, int) {
	switch solver.type_of_move {
	case BAD_MOVE:
		return Move{X: 4, Y: 0, V: 9}, SOLVER_MOVE
	case NO_MOVE:
		return Move{}, SOLVER_STALL
	default:
		return Move{X: 0, Y: 0, V: 5}, SOLVER_MOVE
	}
}

func TestMoves(t *testing.T) {
	grid, err := LoadByLine(easy_sudoku)
	if err != nil {
		log.Fatal(err)
		t.FailNow()
	}
	solver := TestSolver{
		type_of_move: NO_MOVE,
	}
	match := MatchStat{}
	rc := step(solver, &grid, &match, false, false)
	if rc != SOLVER_STALL {
		log.Fatalf("agent is not working, expected a no move instead of %v", rc)
		t.FailNow()
	}
	if len(grid.history) > 0 {
		log.Fatalf("agent is not working, expected len of history 0 instead of %v", len(grid.history))
		t.FailNow()
	}
	solver.type_of_move = BAD_MOVE
	rc = step(solver, &grid, &match, false, false)
	if rc != SOLVER_MISTAKE {
		log.Fatalf("agent is not working, expected a no move due to a mistake instead of %v", rc)
		t.FailNow()
	}
	if len(grid.history) > 0 {
		log.Fatalf("agent is not working, expected len of history 0 instead of %v", len(grid.history))
		t.FailNow()
	}
	solver.type_of_move = GUESS_MOVE
	rc = step(solver, &grid, &match, false, false)
	if rc != SOLVER_MOVE {
		log.Fatalf("agent is not working, expected agent is WIP instead of %v", rc)
		t.FailNow()
	}
	if len(grid.history) != 1 {
		log.Fatalf("agent is not working, expected len of history 1 instead of %v", len(grid.history))
		t.FailNow()
	}
}
