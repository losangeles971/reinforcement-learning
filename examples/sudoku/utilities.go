package sudoku

import (
	"io/ioutil"
	"strings"
)

func LoadBatchFile(batchFile string) ([]string, error) {
	text, err := ioutil.ReadFile(batchFile)
	if err != nil {
		return []string{}, err
	}
	return strings.Split(string(text), "\n"), nil
}