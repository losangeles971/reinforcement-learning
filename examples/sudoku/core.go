package sudoku

import (
	"fmt"
	"log"
	"strconv"
)

const (
	COLUMNS        = 9
	ROWS           = 9
	BLOCKS         = 9
	INVALID_ROW    = 1
	INVALID_COLUMN = 2
	INVALID_BLOCK  = 3
	VALID          = 0
	COMPLETED      = -1
	EMPTY_CELL     = 0
)

type Move struct {
	X int
	Y int
	V int
}

type Sudoku struct {
	matrix  [ROWS][COLUMNS]int
	matrix0 [ROWS][COLUMNS]int
	history []*Move
	moves   int
}

func (s Sudoku) get0(x int, y int) int {
	return s.matrix0[y][x]
}

func (s *Sudoku) set0(move Move) {
	s.matrix0[move.Y][move.X] = move.V
}

func (s Sudoku) Get(x int, y int) int {
	return s.matrix[y][x]
}

/*You can execute a set action only if:
- the action does not change an original not empty cell
Return true if the action runned
*/
func (s *Sudoku) set(move Move, loading bool) bool {
	if s.get0(move.X, move.Y) != EMPTY_CELL {
		return false
	}
	if move.V < 0 || move.V > 9 {
		return false
	}
	s.matrix[move.Y][move.X] = move.V
	if !loading {
		s.history = append(s.history, &Move{move.X, move.Y, move.V})
		s.moves++
	}
	return true
}

func (s Sudoku) HistorySize() int {
	return len(s.history)
}

func (s Sudoku) EqualsTo(grid Sudoku) bool {
	for x := 0; x < COLUMNS; x++ {
		for y := 0; y < ROWS; y++ {
			if s.Get(x, y) != grid.Get(x, y) {
				return false
			}
		}
	}
	return true
}

// Cloning a Sudoku
// Note that: the cloned Sudoku does not have history
func (s Sudoku) Clone() Sudoku {
	cloned := Sudoku{}
	for x := 0; x < COLUMNS; x++ {
		for y := 0; y < ROWS; y++ {
			cloned.set(Move{X: x, Y: y, V: s.Get(x, y)}, true)
		}
	}
	return cloned
}

func (sudoku Sudoku) isValidRow(x int, y int) bool {
	v := sudoku.Get(x, y)
	if v == 0 {
		return true
	}
	occurrence := 0
	for xx := 0; xx < COLUMNS; xx++ {
		if sudoku.Get(xx, y) == v {
			occurrence++
		}
	}
	if occurrence == 1 {
		return true
	} else {
		return false
	}
}

func (sudoku Sudoku) isValidColumn(x int, y int) bool {
	v := sudoku.Get(x, y)
	if v == 0 {
		return true
	}
	occurrence := 0
	for yy := 0; yy < ROWS; yy++ {
		if sudoku.Get(x, yy) == v {
			occurrence++
		}
	}
	if occurrence == 1 {
		return true
	} else {
		return false
	}
}

func (sudoku Sudoku) isValidBlock(x int, y int) bool {
	v := sudoku.Get(x, y)
	if v == 0 {
		return true
	}
	x0 := x - x%3
	y0 := y - y%3
	occurrence := 0
	for xx := 0; xx < 3; xx++ {
		for yy := 0; yy < 3; yy++ {
			if sudoku.Get(x0+xx, y0+yy) == v {
				occurrence++
			}
		}
	}
	if occurrence == 1 {
		return true
	} else {
		return false
	}
}

// this method return an int as return code: COMPLETED, VALID, INVALID_ROW, INVALID_COLUMN, INVALID_BLOCK
func (sudoku Sudoku) Check() int {
	completed := true
	for x := 0; x < COLUMNS; x++ {
		for y := 0; y < ROWS; y++ {
			if sudoku.Get(x, y) == EMPTY_CELL {
				completed = false
			}
			if !sudoku.isValidRow(x, y) {
				return INVALID_ROW
			}
			if !sudoku.isValidColumn(x, y) {
				return INVALID_COLUMN
			}
			if !sudoku.isValidBlock(x, y) {
				return INVALID_BLOCK
			}
		}
	}
	if completed {
		return COMPLETED
	}
	return VALID
}

func (sudoku Sudoku) CheckMove(m Move) int {
	cloned := sudoku.Clone()
	ok := cloned.set(m, false)
	if !ok {
		return INVALID_BLOCK
	}
	return cloned.Check()
}

//returns all possibile values for a given cell
func (s Sudoku) GetMoves(x int, y int) []int {
	moves := []int{}
	if s.Get(x, y) != EMPTY_CELL {
		return moves
	}
	for v := 1; v < 10; v++ {
		cloned := s.Clone()
		cloned.set(Move{X: x, Y: y, V: v}, false)
		vv := cloned.Check()
		if vv == VALID || vv == COMPLETED {
			moves = append(moves, v)
		}
	}
	return moves
}

// return filled, completedRows, completedColumns, completedBlocks
func (sudoku Sudoku) Status() (int, int, int, int) {
	filled := 0
	completedRows := 0
	completedColumns := 0
	completedBlocks := 0
	for y := 0; y < ROWS; y++ {
		row := 0
		for x := 0; x < COLUMNS; x++ {
			if sudoku.Get(x, y) != EMPTY_CELL {
				row++
				filled++
			}
			column := 0
			for yy := 0; yy < ROWS; yy++ {
				if sudoku.Get(x, yy) != EMPTY_CELL {
					column++
				}
			}
			if column == 9 {
				completedColumns++
			}
		}
		if row == 9 {
			completedRows++
		}
	}
	for y := 0; y < ROWS; y += 3 {
		for x := 0; x < COLUMNS; x += 3 {
			if sudoku.BlockStatus(x, y) == 9 {
				completedBlocks++
			}
		}
	}
	return filled, completedRows, completedColumns, completedBlocks
}

func (sudoku Sudoku) BlockStatus(x int, y int) int {
	not_empty := 0
	x0 := x - x%3
	y0 := y - y%3
	for xx := 0; xx < 3; xx++ {
		for yy := 0; yy < 3; yy++ {
			if sudoku.Get(x0+xx, y0+yy) != EMPTY_CELL {
				not_empty++
			}
		}
	}
	return not_empty
}

func (sudoku Sudoku) Print() {
	for y := 0; y < ROWS; y++ {
		row := ""
		for x := 0; x < COLUMNS; x++ {
			row += fmt.Sprintf("%d ", sudoku.Get(x, y))
		}
		log.Println(row)
	}
	log.Print("")
}

func (sudoku Sudoku) ToLine() string {
	line := ""
	for y := 0; y < ROWS; y++ {
		for x := 0; x < COLUMNS; x++ {
			line += fmt.Sprintf("%d", sudoku.Get(x, y))
		}
	}
	return line
}

// Create a Sudoku from a line of ROWSxCOLUMNS numbers
func LoadByLine(data string) (Sudoku, error) {
	if len(data) < (ROWS * COLUMNS) {
		return Sudoku{}, fmt.Errorf("missing cells, they should be %d", ROWS*COLUMNS)
	}
	s := Sudoku{}
	s.history = []*Move{}
	for y := 0; y < ROWS; y++ {
		for x := 0; x < COLUMNS; x++ {
			v, err := strconv.Atoi(data[x+y*COLUMNS : x+y*COLUMNS+1])
			if err != nil {
				return s, err
			}
			m := Move{X: x, Y: y, V: v}
			s.set(m, true)
			s.set0(m)
		}
	}
	return s, nil
}
