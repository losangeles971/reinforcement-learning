package sudoku

import (
	"log"
	"time"
)

type MatchStat struct {
	Steps    int
	Moves    int
	Solved   bool
	Undos    int
	Mistakes int
	Stalled  bool
	Gaveup   bool
	Elapsed  time.Duration
}

func (m MatchStat) Print() {
	log.Printf("solved        %v", m.Solved)
	log.Printf("stalled       %v", m.Stalled)
	log.Printf("gave up!      %v", m.Gaveup)
	log.Printf("steps         %v", m.Steps)
	log.Printf("moves         %v", m.Moves)
	log.Printf("undos         %v", m.Undos)
	log.Printf("mistakes      %v", m.Mistakes)
	log.Printf("elapsed       %v", m.Elapsed.Milliseconds())
}

type BatchStat struct {
	data         []MatchStat
	tot_steps    int
	tot_moves    int
	tot_solved   int
	tot_undos    int
	tot_mistakes int
	tot_stalled  int
	tot_gaveup   int
	tot_elapsed  int64
	avg_steps    float64
	avg_moves    float64
	p_solved     float64
	avg_undos    float64
	avg_mistakes float64
	avg_stalled  float64
	avg_gaveup   float64
	avg_elapsed  float64
}

func CreateBatchStat() BatchStat {
	return BatchStat{
		data:         []MatchStat{},
		tot_steps:    0,
		tot_moves:    0,
		tot_solved:   0,
		tot_undos:    0,
		tot_mistakes: 0,
		tot_stalled:  0,
		tot_gaveup:   0,
		tot_elapsed:  0,
		avg_steps:    0,
		avg_moves:    0,
		p_solved:     0,
		avg_undos:    0,
		avg_mistakes: 0,
		avg_stalled:  0,
		avg_gaveup:   0,
		avg_elapsed:  0,
	}
}

func (b *BatchStat) AddMatchStat(m MatchStat) {
	b.data = append(b.data, m)
}

func (b *BatchStat) Print() {
	log.Printf("runned %v", len(b.data))
	for _, m := range b.data {
		if m.Solved {
			b.tot_solved++
		}
		if m.Gaveup {
			b.tot_gaveup++
		}
		if m.Stalled {
			b.tot_stalled++
		}
		b.tot_moves += m.Moves
		b.tot_undos += m.Undos
		b.tot_elapsed += m.Elapsed.Milliseconds()
		b.tot_mistakes += m.Mistakes
		b.tot_steps += m.Steps
	}
	b.avg_elapsed = float64(b.tot_elapsed / int64(len(b.data)))
	b.avg_steps = float64(b.tot_steps / len(b.data))
	b.avg_moves = float64(b.tot_moves / len(b.data))
	b.p_solved = float64(b.tot_solved/len(b.data)) * 100
	b.avg_undos = float64(b.tot_undos / len(b.data))
	b.avg_mistakes = float64(b.tot_mistakes / len(b.data))
	b.avg_stalled = float64(b.tot_stalled / len(b.data))
	b.avg_gaveup = float64(b.tot_gaveup / len(b.data))
	log.Printf("tot elapsed       %v", b.tot_elapsed)
	log.Printf("tot gave up       %v", b.tot_gaveup)
	log.Printf("tot mistakes      %v", b.tot_mistakes)
	log.Printf("tot moves         %v", b.tot_moves)
	log.Printf("tot solved        %v", b.tot_solved)
	log.Printf("tot stalled       %v", b.tot_stalled)
	log.Printf("tot steps         %v", b.tot_steps)
	log.Printf("tot undos         %v", b.tot_undos)
	log.Printf("avg elapsed       %v", b.avg_elapsed)
	log.Printf("avg gave up       %v", b.avg_gaveup)
	log.Printf("avg mistakes      %v", b.avg_mistakes)
	log.Printf("avg moves         %v", b.avg_moves)
	log.Printf("solved            %v", b.p_solved)
	log.Printf("avg stalled       %v", b.avg_stalled)
	log.Printf("avg steps         %v", b.avg_steps)
	log.Printf("avg undos         %v", b.avg_undos)
}
