package sudoku

import (
	"bufio"
	"log"
	"os"
	"time"
)

const (
	SOLVER_MOVE    = 0
	SOLVER_MISTAKE = 1
	SOLVER_STALL   = 2
	SOLVER_WON     = 3
	SOLVER_GIVEUP  = 4
)

var SOLVER_ACTIONS = []string{
	"MOVE",
	"MISTAKE",
	"STALL",
	"WON",
	"GIVEUP",
}

type Solver interface {
	// grid must be immutable for the agent,
	// so it can be passed by value (or a clone can be passed)
	GetMove(grid Sudoku) (Move, int)
	Save(filepath string) error
	Load(filepath string) error
}

// This method runs a single step from the agent
func step(solver Solver, grid *Sudoku, match *MatchStat, debug bool, trace bool) int {
	vv := grid.Check()
	switch vv {
	case COMPLETED:
		return SOLVER_WON
	default:
		move, rc := solver.GetMove(*grid) //passed by value, grid is immutable for the solver
		switch rc {
		case SOLVER_STALL, SOLVER_WON, SOLVER_GIVEUP:
			// note that the core cannot trust the agent about winning
			if debug {
				log.Printf("move: rc = %s - %v,%v -> %v", SOLVER_ACTIONS[rc], move.X, move.Y, move.V)
			}
			return rc
		case SOLVER_MISTAKE:
			match.Mistakes++
			if debug {
				log.Printf("move: rc = %s - %v,%v -> %v", SOLVER_ACTIONS[rc], move.X, move.Y, move.V)
			}
			return rc
		case SOLVER_MOVE:
			moved := grid.set(move, false)
			if !moved {
				if debug {
					log.Printf("move: rc = %s - %v,%v -> %v", SOLVER_ACTIONS[rc], move.X, move.Y, move.V)
				}
				match.Mistakes++
				return SOLVER_MISTAKE
			}
			if debug {
				log.Printf("move: rc = %s - %v,%v -> %v", SOLVER_ACTIONS[rc], move.X, move.Y, move.V)
			}
			match.Moves++
			if move.V == 0 {
				match.Undos++
			}
			return SOLVER_MOVE
		default:
			log.Fatal("wrong branch of code")
			return SOLVER_GIVEUP
		}
	}
}

// This method runs a whole match of the agent
func solve(data string, solver Solver, max_steps int, debug bool, trace bool) (MatchStat, error) {
	if debug {
		log.Printf("max steps %v", max_steps)
	}
	max_idle := 5
	match := MatchStat{
		Moves:   0,
		Solved:  false,
		Steps:   0,
		Stalled: false,
		Gaveup:  false,
	}
	start := time.Now()
	grid, err := LoadByLine(data)
	if err != nil {
		return match, err
	}
	c := 0
	idle := 0
	for c < max_steps {
		if debug {
			log.Print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
			grid.Print()
		}
		rc := step(solver, &grid, &match, debug, trace)
		c++
		match.Elapsed = time.Since(start)
		match.Steps = c
		switch rc {
		case SOLVER_WON:
			match.Solved = true
			return match, nil
		case SOLVER_STALL, SOLVER_MISTAKE:
			idle++
			if idle > max_idle {
				match.Stalled = true
				return match, nil
			}
		case SOLVER_GIVEUP:
			match.Gaveup = true
			return match, nil
		default:
			idle = 0
		}
		if debug {
			grid.Print()
			if trace {
				log.Print("press enter to continue...")
				bufio.NewReader(os.Stdin).ReadString('\n')
			}
		}
	}
	log.Printf("reached the max number of allowed steps %v", max_steps)
	match.Elapsed = time.Since(start)
	return match, nil
}

/*
Remeber:
likely the Solver's implementation requires a receiver pointer,
therefore if you pass the argument solver by value you'll get a build error
You need to pass a pointer to the Solver's implementation as the solver argument
*/
func ByLine(solver Solver, data string, max_steps int, debug bool, trace bool) (MatchStat, error) {
	return solve(data, solver, max_steps, debug, trace)
}

// Execute a batch of Sudoku grids starting from an array of grids
func Batch(solver Solver, grids []string, max_steps int, debug bool, trace bool) (BatchStat, error) {
	stat := CreateBatchStat()
	for i, data := range grids {
		match, err := solve(data, solver, max_steps, debug, trace)
		log.Printf("match %v of %v terminated", i, len(grids))
		if err != nil {
			return stat, err
		}
		stat.AddMatchStat(match)
	}
	return stat, nil
}

// Execute a batch of Sudoku grids starting from an external file containing the array of grids
func BatchFile(solver Solver, batchFile string, max_steps int, debug bool, trace bool) (BatchStat, error) {
	grids, err := LoadBatchFile(batchFile)
	if err != nil {
		return BatchStat{}, err
	}
	return Batch(solver, grids, max_steps, debug, trace)
}
