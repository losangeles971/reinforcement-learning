package solvers

import (
	"it/losangeles971/rl/sudoku"
	"log"
	"os"
)

type Molly struct {
	excluded *[sudoku.COLUMNS][sudoku.ROWS][10]bool
	queue    []sudoku.Move
}

func (s *Molly) exclude(m sudoku.Move) {
	s.excluded[m.Y][m.X][m.V] = true
}

func (s *Molly) allow(x int, y int, m int) {
	s.excluded[y][x][m] = false
}

func (s Molly) isExcluded(x int, y int, v int) bool {
	return s.excluded[y][x][v]
}

func (s *Molly) getBestMove(grid sudoku.Sudoku) (sudoku.Move, int) {
	move := sudoku.Move{
		X: -1,
		Y: -1,
		V: -1,
	}
	c := 11
	for x := 0; x < sudoku.COLUMNS; x++ {
		for y := 0; y < sudoku.ROWS; y++ {
			if grid.Get(x, y) == sudoku.EMPTY_CELL {
				mm := grid.GetMoves(x, y)
				moves := []int{}
				for _, v := range mm {
					if !s.isExcluded(x, y, v) {
						moves = append(moves, v)
					}
				}
				if len(moves) > 0 && len(moves) < c {
					move.X = x
					move.Y = y
					move.V = moves[0]
					c = len(moves)
				}
			}
		}
	}
	if move.X == -1 {
		return move, sudoku.SOLVER_STALL
	}
	return move, sudoku.SOLVER_MOVE
}

func (solver *Molly) GetMove(grid sudoku.Sudoku) (sudoku.Move, int) {
	for {
		next_move, rc := solver.getBestMove(grid)
		switch rc {
		case sudoku.SOLVER_MOVE:
			solver.queue = append(solver.queue, next_move)
			return next_move, rc
		case sudoku.SOLVER_STALL:
			if len(solver.queue) < 1 {
				log.Print("cannot go back to previous moves, so I give up!")
				return sudoku.Move{}, sudoku.SOLVER_GIVEUP
			}
			last := solver.queue[len(solver.queue)-1]
			solver.queue = solver.queue[:len(solver.queue)-1]
			solver.exclude(last)
		default:
			log.Fatal("wrong branch of code")
			os.Exit(1)
		}
	}
}

func (solver Molly) Save(filepath string) error {
	return nil
}

func (solver Molly) Load(filepath string) error {
	return nil
}

func Create() Molly {
	solver := Molly{
		queue: []sudoku.Move{},
	}
	e := [sudoku.COLUMNS][sudoku.ROWS][10]bool{}
	solver.excluded = &e
	for x := 0; x < sudoku.COLUMNS; x++ {
		for y := 0; y < sudoku.ROWS; y++ {
			for m := 1; m < 10; m++ {
				solver.allow(x, y, m)
			}
		}
	}
	return solver
}
