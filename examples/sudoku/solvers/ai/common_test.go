package ai

import (
	"it/losangeles971/rl/sudoku"
	"log"
	"testing"
)

var easy_sudoku = "000260701680070090190004500820100040004602900050003028009300074040050036703018000"

func TestExcludedMoves(t *testing.T) {
	//000260701
	//680070090
	//190004500
	//820100040
	//004602900
	//050003028
	//009300074
	//040050036
	//703018000
	solver := CreateCactus(0.4, 0.5, 0.1, true)
	grid, _ := sudoku.LoadByLine(easy_sudoku)
	if !solver.firstRun {
		log.Print("solver is not in first run mode")
		t.FailNow()
	}
	grid0 := grid.Clone()
	updateMoves(solver.qlearner.GetActions(), grid0, grid, true)
	for _, action := range solver.qlearner.GetActions() {
		move := action.GetPayload().(sudoku.Move)
		if move.X == 0 && move.Y == 0 && move.V == 0 {
			if !action.IsEnabled() {
				log.Print("undo on 0,0 should be enabled")
				t.FailNow()
			}
		}
		if move.X == 2 && move.Y == 1 && move.V == 5 {
			if !action.IsEnabled() {
				log.Print("set 5 on 2,1 should be enabled")
				t.FailNow()
			}
		}
		if move.X == 4 && move.Y == 1 && move.V == 5 {
			if action.IsEnabled() {
				log.Print("cannot change valut of 4,1")
				t.FailNow()
			}
		}
		if move.X == 0 && move.Y == 0 && move.V == 2 {
			if action.IsEnabled() {
				log.Print("2 -> 0,0 makes grid not valid")
				t.FailNow()
			}
		}
	}
}

func TestSally(t *testing.T) {
	solver := CreateSally(0.9, 0.5, 0.15, true)
	match, err := sudoku.ByLine(&solver, easy_sudoku, 100, true, false)
	if err != nil {
		log.Fatal(err)
		t.FailNow()
	}
	if match.Moves == 0 {
		log.Printf("no moves")
		t.FailNow()
	}
}
