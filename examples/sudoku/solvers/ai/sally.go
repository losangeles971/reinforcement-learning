package ai

//http://phptutorial.info/scripts/sudoku/

import (
	"fmt"
	"it/losangeles971/rl/core"
	"it/losangeles971/rl/sudoku"
	"log"
)

type Sally struct {
	qlearner core.QLearner
	firstRun bool
	debug    bool
	grid0    sudoku.Sudoku
}

func (solver Sally) reward(prev_state string, new_state string) float64 {
	return reward(prev_state, new_state)
}

func (solver *Sally) updateMoves(grid sudoku.Sudoku) {
	updateMoves(solver.qlearner.GetActions(), solver.grid0, grid, true)
}

func (solver *Sally) GetMove(grid sudoku.Sudoku) (sudoku.Move, int) {
	if solver.firstRun {
		solver.grid0 = grid.Clone()
		solver.firstRun = false
	}
	solver.updateMoves(grid)
	state := grid.ToLine()
	action, ok := solver.qlearner.Policy(state)
	if !ok {
		if solver.debug {
			log.Print("no moves from q-learner")
		}
		return sudoku.Move{}, sudoku.SOLVER_STALL
	}
	if !action.IsEnabled() {
		if solver.debug {
			log.Print("a disabled move from q-learner")
		}
		return sudoku.Move{}, sudoku.SOLVER_MISTAKE
	}
	move := action.GetPayload().(sudoku.Move)
	if solver.debug {
		log.Printf("move from qlearner %v aka  %v,%v -> %v", action.GetName(), move.X, move.Y, move.V)
	}
	return move, sudoku.SOLVER_MOVE
}

func (solver Sally) Save(filepath string) error {
	return solver.qlearner.Save(filepath)
}

func (solver Sally) Load(filepath string) error {
	return solver.qlearner.Load(filepath)
}

func CreateSally(alfa float64, gamma float64, epsilon float64, debug bool) Sally {
	agent := Sally{
		debug: debug,
	}
	list := []*core.Action{}
	agent.firstRun = true
	for y := 0; y < sudoku.ROWS; y++ {
		for x := 0; x < sudoku.COLUMNS; x++ {
			for v := 0; v < 10; v++ {
				action := core.Action{}
				action.SetName(fmt.Sprintf("%d%d-%d", x, y, v))
				action.Enable()
				action.SetPayload(sudoku.Move{X: x, Y: y, V: v})
				list = append(list, &action)
			}
		}
	}
	qlearner := core.Create(list, debug)
	qlearner.SetHyperparameters(alfa, gamma, epsilon)
	agent.qlearner = qlearner
	return agent
}
