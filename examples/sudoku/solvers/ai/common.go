package ai

import (
	"fmt"
	"it/losangeles971/rl/core"
	"it/losangeles971/rl/sudoku"
	"log"
	"os"
)

func updateMoves(actions []*core.Action, grid0 sudoku.Sudoku, grid sudoku.Sudoku, excludeBadMoves bool) {
	for _, action := range actions {
		move := action.GetPayload().(sudoku.Move)
		if grid0.Get(move.X, move.Y) != sudoku.EMPTY_CELL {
			//ORIGINALLY FILLED CELL
			action.Disable()
		} else if grid.Get(move.X, move.Y) == move.V {
			//INSANE MOVE (PUT THE SAME NUMBER ALREADY IN PLACE)
			action.Disable()
		} else {
			//NOT ORIGINALLY FILLED CELL
			if excludeBadMoves {
				//disable moves which make the grid not valid
				for v := 1; v < 10; v++ {
					rc := grid.CheckMove(move)
					if rc == sudoku.VALID {
						action.Enable()
					} else {
						action.Disable()
					}
				}
			} else {
				action.Enable()
			}
		}
	}
}

func reward(prev_state string, new_state string) float64 {
	grid1, err := sudoku.LoadByLine(prev_state)
	if err != nil {
		log.Fatalf("agent received a malformed grid from the platform: %v", new_state)
		os.Exit(1)
	}
	grid2, err := sudoku.LoadByLine(new_state)
	if err != nil {
		log.Fatalf("agent received a malformed grid from the platform: %v", new_state)
		os.Exit(1)
	}
	filled1, completedRows1, completedColumns1, completedBlocks1 := grid1.Status()
	filled2, completedRows2, completedColumns2, completedBlocks2 := grid2.Status()
	if completedRows2 > completedRows1 || completedColumns2 > completedColumns1 || completedBlocks2 > completedBlocks1 {
		return 1.0
	}
	if filled2 > filled1 {
		return 0.5
	}
	if filled2 == filled1 {
		return float64(0.2) * float64(filled2/(sudoku.ROWS*sudoku.COLUMNS))
	}
	return 0.0
}

func getActions(stall_action bool) []*core.Action {
	list := []*core.Action{}
	for y := 0; y < sudoku.ROWS; y++ {
		for x := 0; x < sudoku.COLUMNS; x++ {
			for v := 0; v < 10; v++ {
				action := core.Action{}
				action.SetName(fmt.Sprintf("%d%d-%d", x, y, v))
				action.Enable()
				action.SetPayload(sudoku.Move{X: x, Y: y, V: v})
				list = append(list, &action)
			}
		}
	}
	return list
}
