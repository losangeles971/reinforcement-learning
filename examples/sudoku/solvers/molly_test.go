package solvers

import (
	"it/losangeles971/rl/sudoku"
	"log"
	"testing"
)

var easy_sudoku = "000260701680070090190004500820100040004602900050003028009300074040050036703018000"

func TestEasySudoku(t *testing.T) {
	solver := Create()
	match, err := sudoku.ByLine(&solver, easy_sudoku, 100, true, false)
	if err != nil {
		log.Fatal(err)
		t.FailNow()
	}
	if match.Moves == 0 {
		log.Printf("no moves")
		t.FailNow()
	}
	if !match.Solved {
		log.Printf("not solved an easy sudoku")
		t.FailNow()
	}
}
