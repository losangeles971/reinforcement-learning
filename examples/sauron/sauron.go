package sauron

import (
	"losangeles971/rl/business/qlearner"
	"losangeles971/rl/examples/briscola"
	"losangeles971/rl/examples/cardsgame"

	"github.com/sirupsen/logrus"
)

var actions = []string{
	"tira briscola",
	"tira scarto",
	"tira carico",
	"tira punti",
}

const (
	sauron_filepath = "sauron.json"
)

type state struct {
	briscola    int
	mypoints    int
	ho_briscola bool
	ho_scarto   bool
	ho_carico   bool
	ho_punti    bool
	itspoints   int
	itsseed     int
	itsvalue    int
}

type Sauron struct {
	cardsgame.AbstractPlayer
	q       *qlearner.QLearner
	history []*state
}

func (s state) getState() string {
	return ""
}

func New() *Sauron {
	states := []string{}
	for b := 0; b < 4; b++ {
		for myp := 0; myp < 120; myp += 10 {
			for itsp := 0; itsp < 120; itsp += 10 {
				for itss := -1; itss < 4; itss++ {
					for itsv := 0; itsv < 11; itsv++ {
						s := state{
							briscola:  b,
							mypoints:  myp,
							itspoints: itsp,
							itsseed:   itss,
							itsvalue:  itsv,
						}
						states = append(states, qlearner.GetState(s))
					}
				}
			}
		}
	}
	p := Sauron{
		q: qlearner.NewQLearner(
			states,
			actions,
			qlearner.WithAlfa(0.5),
			qlearner.WithEpsilon(0.4),
			qlearner.WithGamma(0.8),
			qlearner.WithFile(sauron_filepath),
		),
		history: []*state{},
	}
	return &p
}

func (p *Sauron) Name() string {
	return "Sauron"
}

func (p *Sauron) IsHuman() bool {
	return false
}

func (p *Sauron) briscola(briscola int) (cardsgame.Card, bool) {
	for i := 0; i < p.Size(); i++ {
		cd := p.GetCard(i)
		if cd.Seed == briscola {
			p.PopCard(cd)
			return cd, true
		}
	}
	return cardsgame.Card{}, false
}

func (p *Sauron) carico(briscola int) (cardsgame.Card, bool) {
	for i := 0; i < p.Size(); i++ {
		cd := p.GetCard(i)
		if cd.Seed != briscola {
			if cd.Value == 1 || cd.Value == 3 {
				p.PopCard(cd)
				return cd, true
			}
		}
	}
	return cardsgame.Card{}, false
}

func (p *Sauron) scarto(briscola int) (cardsgame.Card, bool) {
	for i := 0; i < p.Size(); i++ {
		cd := p.GetCard(i)
		if cd.Seed != briscola && cd.Value != 1 && cd.Value != 3 && cd.Value < 8 {
			p.PopCard(cd)
			return cd, true
		}
	}
	return cardsgame.Card{}, false
}

func (p *Sauron) Play(table cardsgame.Table) cardsgame.Card {
	briscola, _ := table.GetCard(cardsgame.CENTER)
	s := 0
	c := 0
	for i := 0; i < p.Size(); i++ {
		cd := p.GetCard(i)
		if cd.Value == 1 || cd.Value == 3 {
			c++
		} else if cd.Value < 8 {
			s++
		}
	}
	last := &state{
		name:      getState(briscola.Seed, p.points, s, c),
		attacking: true,
		briscola:  briscola.Seed,
	}
	var err error
	last.action, err = p.q.Policy(last.name)
	if err != nil {
		logrus.Errorf("policy failed -> %v", err)
	}
	logrus.Debugf("Pdor chose [ %v ]", last.action)
	var played_card *cardsgame.Card
	switch last.action {
	case "briscola":
		cd, ok := p.briscola(briscola.Seed)
		if ok {
			played_card = &cd
		}
	case "scarto":
		cd, ok := p.scarto(briscola.Seed)
		if ok {
			played_card = &cd
		}
	case "carico":
		cd, ok := p.carico(briscola.Seed)
		if ok {
			played_card = &cd
		}
	}
	if played_card == nil {
		cd := p.GetCard(0)
		p.PopCard(cd)
		played_card = &cd
	}
	last.mycard = *played_card
	p.history = append(p.history, last)
	return last.mycard
}

func (p *Pdor) TellYouHandResult(table cardsgame.Table, result bool) {
	last := p.history[len(p.history)-1]
	last.won = result
	if len(p.history) > 1 {
		previous := p.history[len(p.history)-2]
		nord, _ := table.GetLocation(cardsgame.NORD)
		sud, _ := table.GetLocation(cardsgame.SUD)
		c1, _ := nord.GetCard()
		c2, _ := sud.GetCard()
		point := briscola.CardPoints[c1.Value] + briscola.CardPoints[c2.Value]
		var reward = 0.0
		if result {
			if point == 0 {
				reward = 0.4
			} else {
				reward = 0.5
			}
		} else {
			if point == 0 {
				reward = 0.3
			} else {
				reward = 0.1
			}
		}
		logrus.Debugf("Pdor is learning from [ %s ][ %s ]-[ %s ]>[ %v ]", previous.name, last.action, last.name, reward)
		err := p.q.Learn(previous.name, last.action, last.name, reward)
		if err != nil {
			logrus.Errorf("Pdor is NOT learning -> [ %v ]", err)
		}
	}
}

func (p *Pdor) TellYouMatchResult(result bool) {
	if len(p.history) > 1 {
		last := p.history[len(p.history)-1]
		previous := p.history[len(p.history)-2]
		var reward = 0.0
		if result {
			reward = 1.0
		} else {
			reward = 0.0
		}
		err := p.q.Learn(previous.name, last.action, last.name, reward)
		if err != nil {
			logrus.Errorf("Pdor is NOT learning -> [ %v ]", err)
		}
	}
}

func (p *Pdor) Save() error {
	return p.q.Save()
}
