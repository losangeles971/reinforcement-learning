module losangeles971/rl

go 1.17

require (
	github.com/spf13/cobra v1.1.1
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)

require (
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/looplab/fsm v0.3.0 // indirect
	github.com/sirupsen/logrus v1.8.1 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	golang.org/x/sys v0.0.0-20191026070338-33540a1f6037 // indirect
)
