package qlearner

import (
	"testing"
)

func TestLearn(t *testing.T) {
	alfa := 0.1
	gamma := 0.2
	epsilon := 0.3
	prev_state := states_test[0]
	new_state := states_test[1]
	act := actions_test[0]
	prev_qv := 0.5
	reward := 0.5
	q := NewQLearner(states_test, actions_test, WithAlfa(alfa), WithEpsilon(epsilon), WithGamma(gamma))
	new_maxqv, err := q.Experience.getMaxQValue(new_state)
	if err != nil {
		t.Fatal(err)
	}
	err = q.Experience.setQValue(prev_state, act, prev_qv)
	if err != nil {
		t.Fatal(err)
	}
	exp_updated_qv := prev_qv + q.alfa*(reward+q.gamma*new_maxqv-prev_qv)
	err = q.Learn(prev_state, act, new_state, reward)
	if err != nil {
		t.Fatal(err)
	}
	updated_qv, err := q.Experience.getQValue(prev_state, act)
	if err != nil {
		t.Fatal(err)
	}
	if updated_qv != exp_updated_qv {
		t.Fatalf("expected update qv is %v not %v", exp_updated_qv, updated_qv)
	}
}