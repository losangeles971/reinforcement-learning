package qlearner

import (
	"testing"
)

var states_test = []string{"s1", "s2",}

func TestExperience(t *testing.T) {
	exp := newExperience(states_test,actions_test)
	for s := range states_test {
		for a := range actions_test {
			err := exp.setQValue(states_test[s], actions_test[a], float64(a))
			if err != nil {
				t.Fatal(err)
			}
		}			
	}
	expected := float64(len(actions_test) - 1)
	for s := range states_test {
		qvMax, err := exp.getMaxQValue(states_test[s])
		if err != nil {
			t.Fatal(err)
		}
		if qvMax != expected {
			t.Errorf("expected max value %v not %v", expected, qvMax)
		}
	}
	_, err := exp.getQValue(states_test[0], "")
	t.Log(err)
	if err == nil {
		t.Error("expected an error for missing action")
	}
}