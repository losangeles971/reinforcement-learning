package qlearner

import (
	"os"
	"testing"
)

func TestIO(t *testing.T) {
	alfa := 0.1
	gamma := 0.2
	epsilon := 0.3
	filepath := os.TempDir() + "/test.json"
	q := NewQLearner(states_test, actions_test, WithAlfa(alfa), WithEpsilon(epsilon), WithGamma(gamma), WithFile(filepath))
	for s := range states_test {
		for a := range actions_test {
			err := q.Experience.setQValue(states_test[s], actions_test[a], float64(a))
			if err != nil {
				t.Fatal(err)
			}
		}			
	}
	err := q.Save()
	if err != nil {
		t.Fatal(err)
	}
	q2 := NewQLearner(states_test, actions_test, WithAlfa(alfa), WithEpsilon(epsilon), WithGamma(gamma), WithFile(filepath))
	err = q2.Load()
	if err != nil {
		t.Fatal(err)
	}
	for s := range states_test {
		for a := range actions_test {
			qv, err := q.Experience.getQValue(states_test[s], actions_test[a])
			if err != nil {
				t.Fatal(err)
			}
			if qv != float64(a) {
				t.Errorf("expected %v not %v", float64(a), qv)
			}
		}			
	}
}