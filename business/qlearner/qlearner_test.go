package qlearner

import (
	"testing"
)

func TestCreateQLearner(t *testing.T) {
	alfa := 0.1
	gamma := 0.2
	epsilon := 0.3
	q := NewQLearner(states_test, actions_test, WithAlfa(alfa), WithEpsilon(epsilon), WithGamma(gamma))
	if q.alfa != alfa {
		t.Errorf("alfa must be %v not %v", alfa, q.alfa)
		t.Errorf("gamma must be %v not %v", gamma, q.gamma)
		t.Errorf("epsilon must be %v not %v", epsilon, q.epsilon)
	}
}