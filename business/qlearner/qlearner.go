package qlearner

import (
	"os"

	log "github.com/sirupsen/logrus"
)

// QLearner includes all driving data to learn.
type QLearner struct {
	Experience *Experience `json:"experience"`
	alfa       float64 
	gamma      float64 
	epsilon    float64
	filepath   string
}

type QLearnerOpt func(*QLearner)

func WithAlfa(alfa float64) QLearnerOpt {
	return func(q *QLearner) {
		q.alfa = alfa
	}
}

func WithEpsilon(epsilon float64) QLearnerOpt {
	return func(q *QLearner) {
		q.epsilon = epsilon
	}
}

func WithGamma(gamma float64) QLearnerOpt {
	return func(q *QLearner) {
		q.gamma = gamma
	}
}

func WithFile(filepath string) QLearnerOpt {
	return func(q *QLearner) {
		q.filepath = filepath
		if _, err := os.Stat(q.filepath); err == nil {
			err := q.Load()
			if err != nil {
				log.Warnf("failed to load experience from file %s -- %v", q.filepath, err)
			}
		} else {
			log.Warnf("experience file  %s does not exist", q.filepath)
		}
	}
}

// This  method creates a new QLearning object from a given list of possible actions
func NewQLearner(states []string, actions []string, opts ...QLearnerOpt) *QLearner {
	q := &QLearner{
		Experience: newExperience(states, actions),
		alfa:       0.5,
		gamma:      0.5,
		epsilon:    0.5,
		filepath:   "",
	}
	for _, opt := range opts {
		opt(q)
	}
	return q
}
