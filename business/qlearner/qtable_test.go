package qlearner

import (
	"testing"
)

var actions_test = []string{"a1", "a2",}

func TestQTable(t *testing.T) {
	qt := newQTable(actions_test)
	for a := range actions_test {
		qt.setQValue(actions_test[a], float64(a))
	}
	for a := range actions_test {
		qv, err := qt.getQValue(actions_test[a])
		if err != nil {
			t.Fatal(err)
		}
		if qv != float64(a) {
			t.Errorf("expected value for %s is %v not %v", actions_test[a], float64(a), qv)
		}
	}
	qvMax := qt.getMaxQValue()
	expected := float64(len(actions_test) - 1)
	if qvMax != expected {
		t.Errorf("expected max value %v not %v", expected, qvMax)
	}
	_, err := qt.getQValue("")
	t.Log(err)
	if err == nil {
		t.Error("expected an error for missing action")
	}
}