package qlearner

import (
	"fmt"
	"math/rand"

	"github.com/sirupsen/logrus"
)

// exploiting selects the best action for the given state
func (q *QLearner) exploiting(state string) (string, error) {
	var best_action string
	action_qv := -1.0
	qtable, err := q.Experience.getQTable(state)
	if err != nil {
		return "", err
	}
	for action, qvalue := range qtable.Qvalues {
		if qvalue > action_qv {
			best_action = action
			action_qv = qvalue
		}
	}
	if action_qv == -1.0 {
		return "", fmt.Errorf("no possible action for state %s", state)
	}
	return best_action, nil
}

// exploring chooses a random action.
func (q *QLearner) exploring(state string) (string, error) {
	qtable, err := q.Experience.getQTable(state)
	if err != nil {
		return "", err
	}
	enabled := []string{}
	for action := range qtable.Qvalues {
		enabled = append(enabled, action)
	}
	return enabled[rand.Intn(len(enabled))], nil
}

// Policy returns the next action of the agent depending on the current state.
func (q QLearner) Policy(state string) (string, error) {
	logrus.Tracef("policy [ %s ]-?", state)
	r := rand.Float64()
	if r <= q.epsilon {
		return q.exploring(state)
	} else {
		return q.exploiting(state)
	}
}

// Learn updates the QTables depending on the last executed action and the reward.
func (q *QLearner) Learn(prev_state string, executed_action string, new_state string, reward float64) error {
	logrus.Tracef("learn [ %s ][ %s ]-[ %s ]>[ %v ]", prev_state, executed_action, new_state, reward)
	current_qv, err := q.Experience.getQValue(prev_state, executed_action)
	if err != nil {
		return err
	}
	maxQValue, err := q.Experience.getMaxQValue(new_state)
	if err != nil {
		return err
	}
	updated_qv := current_qv + q.alfa*(reward+q.gamma*maxQValue-current_qv)
	return q.Experience.setQValue(prev_state, executed_action, updated_qv)
}