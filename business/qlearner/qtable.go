package qlearner

import (
	"fmt"
	"math/rand"

	"github.com/sirupsen/logrus"
)

// Q-Table implements the association map: action -> q-value.
type QTable struct {
	Qvalues map[string]float64 `json:"qvalues"` // action -> QValue
}

func newQTable(actions []string) *QTable {
	qtable := QTable{
		Qvalues: map[string]float64{},
	}
	for a := range actions {
		qtable.Qvalues[actions[a]] = rand.Float64()
	}
	return &qtable
}

func (q *QTable) getQValue(action string) (float64, error) {
	_, ok := q.Qvalues[action]
	logrus.Tracef("getQValue [ %s ] is [ %v ]", action, ok)
	if !ok {
		return 0, fmt.Errorf("action %s has no QValue", action)
	}
	return q.Qvalues[action], nil
}

func (q *QTable) setQValue(action string, qvalue float64) error {
	_, ok := q.Qvalues[action]
	if !ok {
		return fmt.Errorf("action %s has no QValue", action)
	}
	q.Qvalues[action] = qvalue
	return nil
}

func (q *QTable) getMaxQValue() float64 {
	max := 0.0
	// loop over all possible actions
	for _, qvalue := range q.Qvalues {
		if qvalue > max {
			max = qvalue
		}
	}
	return max
}