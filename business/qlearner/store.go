package qlearner

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)

func (q *QLearner) Save() error {
	if len(q.filepath) < 1 {
		return fmt.Errorf("missing filepath for QLearner [%s]", q.filepath)
	}
	data, err := json.Marshal(q.Experience)
	if err != nil {
		return err
	}
	return ioutil.WriteFile(q.filepath, data, 0755)
}

func (q *QLearner) Load() error {
	if len(q.filepath) < 1 {
		return fmt.Errorf("missing filepath for QLearner [%s]", q.filepath)
	}
	data, err := ioutil.ReadFile(q.filepath)
	if err != nil {
		return err
	}
	return json.Unmarshal(data, q.Experience)
}