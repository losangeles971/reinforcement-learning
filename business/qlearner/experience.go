package qlearner

import (
	"fmt"
	"crypto/sha256"
)

// Experience implements the association map: state -> Q-Table.
type Experience struct {
	QTables map[string]*QTable `json:"qtables"`
}

func newExperience(states []string, actions []string) *Experience {
	exp := Experience{
		QTables: map[string]*QTable{},
	}
	for i := range states {
		exp.QTables[states[i]] = newQTable(actions)
	}
	return &exp
}

func (q *Experience) getQTable(state string) (*QTable, error) {
	_, ok := q.QTables[state]
	if !ok {
		return nil, fmt.Errorf("state %s has not QTable", state)
	}
	return q.QTables[state], nil
}

func (q *Experience) getQValue(state string, action string) (float64, error) {
	qtable, err := q.getQTable(state)
	if err != nil {
		return 0, err
	}
	return qtable.getQValue(action)
}

func (q *Experience) setQValue(state string, action string, qvalue float64) error {
	qtable, err := q.getQTable(state)
	if err != nil {
		return err
	}
	return qtable.setQValue(action, qvalue)
}

// Given a state, this method returns the max possible QValue (depending on actions)
func (q *Experience) getMaxQValue(state string) (float64, error) {
	qtable, err := q.getQTable(state)
	if err != nil {
		return 0, err
	}
	return qtable.getMaxQValue(), nil
}

func GetState(o interface{}) string {
	h := sha256.New()
    h.Write([]byte(fmt.Sprintf("%v", o)))
    return fmt.Sprintf("%x", h.Sum(nil))
}