package cmd

import (
	"log"
	"os"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
	Use:   "rl",
	Short: "Reinforcement learning",
	Long: `Reinforcement learning.
`,
	Run: func(cmd *cobra.Command, args []string) {
	},
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		log.Panic(err)
		os.Exit(1)
	}
}

func initConfig() {
	logrus.SetLevel(logrus.TraceLevel)
	os.Remove("briscola.log")
	f, err := os.OpenFile("briscola.log", os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0644)
	if err == nil {
		logrus.SetOutput(f)
	}
}

func init() {
	cobra.OnInitialize(initConfig)
}
