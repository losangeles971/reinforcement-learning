package cmd

import (
	"fmt"
	"losangeles971/rl/examples/briscola"
	"losangeles971/rl/examples/cardsgame"
	"losangeles971/rl/examples/pdor"

	"github.com/spf13/cobra"
)

func getPlayer(name string) cardsgame.Player {
	switch name {
	case "human":
		return cardsgame.NewTestPlayer(true)
	case "pdor":
		return pdor.New()
	default:
		return cardsgame.NewTestPlayer(false)
	}
}

func savePlayer(pp cardsgame.Player) {
	fmt.Printf("check if it is possible to save experience of %s\n", pp.Name())
	switch pp.Name() {
	case "Pdor":
		fmt.Printf("saving experience of %s\n", pp.Name())
		dd := pp.(*pdor.Pdor)
		err := dd.Save()
		if err != nil {
			fmt.Printf("failed to save experience of %s -> %v \n", pp.Name(), err)
		}
	}
}

var nord string
var sud string
var matches int
var stepper bool
var store bool

var briscolaCmd = &cobra.Command{
	Use:   "briscola",
	Short: "Reinforcement learning and Briscola",
	Long: `Reinforcement learning and Briscola.
`,
	Run: func(cmd *cobra.Command, args []string) {
		nord_won := 0
		sud_won := 0
		for i := 0; i < matches; i++ {
			nordPlayer := getPlayer(nord)
			sudPlayer := getPlayer(sud)
			if nordPlayer.IsHuman() || sudPlayer.IsHuman() {
				stepper = true
				matches = 1
			}
			fmt.Printf("match number %v\n", i+1)
			b, err := briscola.New(stepper)
			if err != nil {
				panic(err)
			}
			err = b.AddPlayerToLocation(nordPlayer, cardsgame.NORD)
			if err != nil {
				panic(err)
			}
			err = b.AddPlayerToLocation(sudPlayer, cardsgame.SUD)
			if err != nil {
				panic(err)
			}
			err = b.Prepare()
			if err != nil {
				panic(err)
			}
			err = b.Run()
			if err != nil {
				panic(fmt.Errorf("error at round %v -> %v", i, err))
			}
			b.PrintWinner()
			for _, ww := range b.Winners() {
				if ww.Name() == nordPlayer.Name() {
					nord_won++
				}
				if ww.Name() == sudPlayer.Name() {
					sud_won++
				}
			}
			savePlayer(nordPlayer)
			savePlayer(sudPlayer)
		}
		fmt.Printf("Player %s won %v matches", nord, nord_won)
		fmt.Printf("Player %s won %v matches", sud, sud_won)
	},
}

func init() {
	briscolaCmd.PersistentFlags().StringVar(&nord, "nord", "test", "name of nord player")
	briscolaCmd.PersistentFlags().StringVar(&sud, "sud", "test", "name of sud player")
	briscolaCmd.PersistentFlags().IntVar(&matches, "matches", 1, "number of matches")
	briscolaCmd.PersistentFlags().BoolVar(&stepper, "stepper", false, "stepper")
	briscolaCmd.PersistentFlags().BoolVar(&store, "store", true, "store experience")
	rootCmd.AddCommand(briscolaCmd)
}
